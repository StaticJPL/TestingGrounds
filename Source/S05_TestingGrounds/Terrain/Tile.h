// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ActorPool.h"
#include "Tile.generated.h"

USTRUCT(BlueprintType)
struct FSpawnPosition {

	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Location;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Rotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Scale;

};

USTRUCT(BlueprintType)
struct FSpawnValues {

	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MinSpawn = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MaxSpawn = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Radius = 500;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MinScale = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxScale = 1.f;

};


class UActorPool;

UCLASS()
class S05_TESTINGGROUNDS_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	UFUNCTION(BlueprintCallable)
	void PlaceActors(TSubclassOf<AActor> ToSpawn, const FSpawnValues& SpawnValues);

	UFUNCTION(BlueprintCallable, Category = "Spawning")
	void PlaceAIPawns(TSubclassOf<APawn> ToSpawn, const FSpawnValues& SpawnValues);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayREason) override;

	UPROPERTY(EditDefaultsOnly, Category = "Navigation")
	FVector NavigationBoundsOffset;

	UPROPERTY(EditDefaultsOnly,Category="Spawning")
	FVector MinExtent;

	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	FVector MaxExtent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Pool")
	void SetPool(UActorPool* InPool);


private:
	bool FindEmptyLocation(FVector& OutLocation, float Radius);
	
	void PlaceActor(TSubclassOf<AActor> ToSpawn, const FSpawnPosition& SpawnPosition);

	void PlaceActor(TSubclassOf<APawn> ToSpawn, const FSpawnPosition& SpawnPosition);
	
	bool CanSpawnAtLocation(FVector Location, float Radius);
	
	void PositionNavMeshBoundsVolume();


	template<class T>
	void RandomlyPlaceActors(TSubclassOf<T> ToSpawn, const FSpawnValues& SpawnValues);

	UActorPool* Pool;

	AActor* NavMeshBoundsVolume;


};
