// Fill out your copyright notice in the Description page of Project Settings.

#include "S05_TestingGrounds.h"
#include "NavMesh/NavMeshBoundsVolume.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "ActorPool.h"
#include "InfiniteTerrainGameMode.h"

AInfiniteTerrainGameMode::AInfiniteTerrainGameMode() {

	NavMeshBoundsVolumePool = CreateDefaultSubobject<UActorPool>(FName("Nav Mesh Bounds Volume Pool"));
}


void AInfiniteTerrainGameMode::PopulateBoundsVolumePool() 
{

	auto VolumeIterator = TActorIterator<ANavMeshBoundsVolume>(GetWorld());
	
	while (VolumeIterator) {
		AddToPool(*VolumeIterator);
		++VolumeIterator;
	}
}


void AInfiniteTerrainGameMode::AddToPool(ANavMeshBoundsVolume *VolumeToAdd) 
{
	NavMeshBoundsVolumePool->Add(VolumeToAdd);
	UE_LOG(LogTemp, Warning, TEXT("Found Actor:%s"), *VolumeToAdd->GetName());

}